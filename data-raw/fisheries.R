## code to prepare `fisheries` dataset goes here
fisheries <- read.csv("./inst/extdata/fisheries.csv", stringsAsFactors = FALSE)
usethis::use_data(fisheries, overwrite = TRUE)
