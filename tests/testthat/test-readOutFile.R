test_that("read single OUT file", {
  out_filename <- file.path(system.file("extdata", package = "hrjexport"), "COW100CYR.OUT")
  expect_equal(readOutFiles(out_filename), testSingleOutFile)
})


test_that("read zipped OUT files", {
  out_filename <- file.path(system.file("extdata", package = "hrjexport"), "CHI_SAMPLE.zip")
  expect_equal(readOutFiles(out_filename), testZipOutFile)
})


test_that("export zipped OUT files", {
  out_filename <- file.path(system.file("extdata", package = "hrjexport"), "CHI_SAMPLE.zip")
  expect_equal(exportOutFiles(out_filename), testZipOutExport)
})
