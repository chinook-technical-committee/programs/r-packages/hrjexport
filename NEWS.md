# hrjexport 2.5.0

* Add switch to filter to either marked or unmarked HRJ files when reading

# hrjexport 2.4.0

* Modify HRJ parsing to support both fixed width (REAM) and space delimited (SIT4)

# hrjexport 2.3.1

* Support new HRJ file format with more precision on escapement values
* Clear warnings by using new `cross_join(...)` function in dplyr
* Fix bug several bugs

# hrjexport 2.3.0

* Add a `compareHrjFilesLong(...)` function to compare HRJ results from REAM

# hrjexport 2.2.0

* Add function to compare HRJ files directly using `compareHrjFiles(...)`

# hrjexport 2.1.1

* Modify default parameter of `exportOutExcelFiles` to export both Calendar and Brood Year data

# hrjexport 2.1.0

* Modified to use `Terminal Ocean Net Age` parsed from the OUT file
* Fix calculation for `Marine Survival` as `Cohort Size BNM` / `cwt_rel_total`
* Add tests for HRJ and OUT file export
* Add default file name for export HRJ and OUT file

# hrjexport 2.0.0

* Add support to export OUT files into a single Total Mortality table


# hrjexport 1.0.0

* Added a `NEWS.md` file to track changes to the package.
* Finalized to release version 1.0.0
