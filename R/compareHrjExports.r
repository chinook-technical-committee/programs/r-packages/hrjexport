
#' Compare two HRJ excel exports and produce an excel file of the differences
#'
#' @param first_hrj_excel Path to the first excel filename with HRJ data
#' @param second_hrj_excel Path to the second excel filename with HRJ data
#' @param first_hrj_worksheet Name of the worksheet with the first HRJ data
#' @param second_hrj_worksheet Name of the worksheet with the second HRJ data
#' @param output_file_name File name to write comparison too
#'
#' @export
#'
#' @importFrom openxlsx read.xlsx
#'
compareHrjExports <- function(first_hrj_excel,
                              second_hrj_excel,
                              first_hrj_worksheet = "HRJ",
                              second_hrj_worksheet = "HRJ",
                              output_file_name = NULL) {
  if (!requireNamespace(MrpUtilPkgName, quietly = TRUE)) {
    stop(glue("Package \"{MrpUtilPkgName}\" needed for this function to work. Please install it."),
         call. = FALSE)
  }

  first_df <-
    read.xlsx(first_hrj_excel,
              sheet = first_hrj_worksheet) %>%
    as_tibble()

  second_df <-
    read.xlsx(second_hrj_excel,
              sheet = second_hrj_worksheet) %>%
    as_tibble()

  if (is.null(output_file_name)) {
    output_file_name <- "HRJ_diff.xlsx"
  }

  mrpBaseUtils::compareDataFramesOutput(first_df,
                                        second_df,
                                        key_cols = c("stock_code",
                                                     "brood_year",
                                                     "age",
                                                     "fishery_id"),
                                        output_file_name = output_file_name,
                                        output_format = mrpBaseUtils::MrpOutputExcel,
                                        numeric_diff = rep(list("abs"), length(first_df)),
                                        title = "Compare HRJ Exports")
}


#' Compare two sets of HRJ files and produce an excel file of the differences
#'
#' @param first_hrj_files Path to the first HRJ filename with HRJ data
#' @param second_hrj_files Path to the second HRJ filename with HRJ data
#' @param output_file_name File name to write comparison too
#'
#' @export
#'
#' @importFrom openxlsx read.xlsx
#'
#' @examples
#' compareHrjFiles(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
#'                 system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
#'                 output_file_name = tempfile(fileext=".xlsx"))
#'
compareHrjFiles <- function(first_hrj_files,
                            second_hrj_files,
                            output_file_name = NULL) {
  if (!requireNamespace(MrpUtilPkgName, quietly = TRUE)) {
    stop(glue("Package \"{MrpUtilPkgName}\" needed for this function to work. Please install it."),
         call. = FALSE)
  }

  first_hrj_list <-
    readHrjFiles(first_hrj_files)

  second_hrj_list <-
    readHrjFiles(second_hrj_files)


  first_hrj_list$brood_hrj <-
    first_hrj_list$brood_hrj |>
    mutate(across(c(lc_aeq,tm_aeq,lc_nom,tm_nom,canada_stray_esc,us_stray_esc,escapement), as.double))

  first_hrj_list$calendar_hrj <-
    first_hrj_list$calendar_hrj |>
    mutate(across(c(lc_aeq,tm_aeq,lc_nom,tm_nom,canada_stray_esc,us_stray_esc,escapement), as.double))

  second_hrj_list$brood_hrj <-
    second_hrj_list$brood_hrj |>
    mutate(across(c(lc_aeq,tm_aeq,lc_nom,tm_nom,canada_stray_esc,us_stray_esc,escapement), as.double))

  second_hrj_list$calendar_hrj <-
    second_hrj_list$calendar_hrj |>
    mutate(across(c(lc_aeq,tm_aeq,lc_nom,tm_nom,canada_stray_esc,us_stray_esc,escapement), as.double))

  if (is.null(output_file_name)) {
    output_file_name <- "HRJ_diff.xlsx"
  }

  hrj_key_cols <- c("stock_code",
                    "brood_year",
                    "age",
                    "fishery_id")

  mrpBaseUtils::compareDataFramesOutput(first_hrj_list,
                                        second_hrj_list,
                                        key_cols = list(hrj_key_cols, hrj_key_cols),
                                        output_file_name = output_file_name,
                                        output_format = mrpBaseUtils::MrpOutputExcel,
                                        numeric_diff = "abs",
                                        title = c("Brood HRJ Files", "Calendar HRJ Files"))
}



#' Compare HRJ Files (Long Form)
#'
#' Compare two sets of HRJ files and produce an csv file in long format of the differences
#'
#' @param first_hrj_files Path to the first HRJ filename with HRJ data
#' @param second_hrj_files Path to the second HRJ filename with HRJ data
#'
#' @export
#'
#' @importFrom tidyr pivot_longer
#'
#' @examples
#' compareHrjFilesLong(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
#'                     system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"))
#'
compareHrjFilesLong <- function (first_hrj_files,
                                 second_hrj_files)
{
  first_hrj_list <- readHrjFiles(first_hrj_files) # read in 1st set of HRJs
  second_hrj_list <- readHrjFiles(second_hrj_files) # read in 2nd set of HRJs

  # extract calendar and brood year data from list of HRJs and combine into dataframe
  first_hrj_comb <-
    bind_rows(first_hrj_list$calendar_hrj |>
                mutate(yrType = 'CY'),
              first_hrj_list$brood_hrj |>
                mutate(yrType = 'BY')) |>
    # change all values to double/numeric:
    mutate(across(c(lc_aeq,
                    tm_aeq,
                    lc_nom,
                    tm_nom,
                    cs_or_ts,
                    canada_stray_esc,
                    us_stray_esc,
                    escapement), as.double))
  # convert to  long format:
  first_hrj_long <- first_hrj_comb |>
    # remove escapement and stray columns as estimated at the stock rather than fishery level,
    # same goes for rows/fisheries 78 and 79, as these are duplicate rows for CA and US strays, respectively
    select(-c(escapement,
              canada_stray_esc,
              us_stray_esc)) |>
    filter(fishery_id %notin% c(78, 79)) |>
    pivot_longer(cols = c('lc_aeq',
                          'tm_aeq',
                          'lc_nom',
                          'tm_nom',
                          'cs_or_ts'),
                 names_to = 'data.type',
                 values_to = 'first_est') |>
    # bind with long format data for escapement and strays
    bind_rows(
      first_hrj_comb |>
        distinct(yrType,
                 brood_year,
                 age,
                 stock_code,
                 escapement,
                 canada_stray_esc,
                 us_stray_esc) |>
        pivot_longer(cols = c('escapement',
                              'canada_stray_esc',
                              'us_stray_esc'),
                     names_to = 'data.type',
                     values_to = 'first_est') |>
        mutate(fishery_id = ifelse(data.type == 'escapement', 0,
                                   ifelse(data.type == 'canada_stray_esc', 78, 79)))
    )
  # same as above but for 2nd set of HRJs
  second_hrj_comb <-
    bind_rows(second_hrj_list$calendar_hrj |>
                mutate(yrType = 'CY'),
              second_hrj_list$brood_hrj |>
                mutate(yrType = 'BY')) |>
    mutate(across(c(lc_aeq,
                    tm_aeq,
                    lc_nom,
                    tm_nom,
                    cs_or_ts,
                    canada_stray_esc,
                    us_stray_esc,
                    escapement), as.double))

  second_hrj_long <- second_hrj_comb |>
    select(-c(escapement,
              canada_stray_esc,
              us_stray_esc)) |>
    filter(fishery_id %notin% c(78, 79)) |>
    pivot_longer(cols = c('lc_aeq',
                          'tm_aeq',
                          'lc_nom',
                          'tm_nom',
                          'cs_or_ts'),
                 names_to = 'data.type',
                 values_to = 'second_est') |>
    bind_rows(
      second_hrj_comb |>
        distinct(yrType,
                 brood_year,
                 age,
                 stock_code,
                 escapement,
                 canada_stray_esc,
                 us_stray_esc) |>
        pivot_longer(cols = c('escapement',
                              'canada_stray_esc',
                              'us_stray_esc'),
                     names_to = 'data.type',
                     values_to = 'second_est') |>
        mutate(fishery_id = ifelse(data.type == 'escapement', 0,
                                   ifelse(data.type == 'canada_stray_esc', 78, 79)))
    )
  # join the two data frames together and calculate differences:
  compareHrj <-
    full_join(first_hrj_long,
              second_hrj_long) |>
    mutate(status = case_when(first_est == second_est ~ 'same',
                              first_est != second_est ~ 'different',
                              is.na(first_est) & !is.na(second_est) ~ 'missing_1st',
                              !is.na(first_est) & is.na(second_est) ~ 'missing_2nd',
                              is.na(first_est) & is.na(second_est) ~ 'missing_both'),
           diff = coalesce(first_est, 0) - coalesce(second_est, 0),
           abs.diff = abs(diff),
           perc.diff = ifelse(diff == 0, 0, abs(first_est-second_est)/((first_est+second_est)/2)))
  return(compareHrj)
}
