#' Format a vector of years into decade text (e.g. 1989 to 80s)
#'
#' @param years year values to convert to decades
#'
#' @return A character vector of decade identifiers
#'
formatYrDecade <- function(years) {
  return(paste0(years %% 100 %/% 10, "0s"))
}

#' Format an OUT Data frame with stock summary fields for export
#'
#' @param out_df OUT data frame to format for exports
#'
#' @return A OUT data frame with additional columns describing the OUT data
#'
#' @importFrom dplyr mutate tibble right_join inner_join if_else case_when
#' @importFrom dplyr everything rename n
#' @importFrom tidyr pivot_longer pivot_wider
#'
formatOutStockExport <- function(out_df) {
  stock_country <-
    hrjexport::Stocks %>%
    select(stock_country,
           stock_code)

  stock_ages <-
    out_df %>%
    distinct(stock_code, age) %>%
    group_by(stock_code) %>%
    summarize(`Stock MinAge`= min(age),
              `Stock NAges` = n(),
              max_age = max(age)) %>%
    ungroup()

  stock_brood_ages <-
    out_df %>%
    group_by(stock_code, brood_year) %>%
    summarize(brood_max_age = max(age)) %>%
    ungroup() %>%
    inner_join(stock_ages, by="stock_code") %>%
    mutate(`CompleteBrYr` = if_else(brood_max_age == max_age, "Y", "N"),
           LastBrRY = brood_max_age + brood_year) %>%
    select(stock_code, brood_year, `CompleteBrYr`, LastBrRY)

  out_df <-
    out_df %>%
    right_join(stock_country, ., by="stock_code") %>%
    left_join(Ages, by = c(age = "age")) %>%
    left_join(stock_ages, by="stock_code") %>%
    left_join(stock_brood_ages, by=c("stock_code", "brood_year")) %>%
    mutate(`Youngest Age` = if_else(`Stock MinAge` == age, "Y", "N"), .before = `Stock MinAge`) %>%
    mutate(run_year = brood_year + age, .before = brood_year) %>%
    mutate(ProdExpFactor = rel_total / cwt_rel_total,
           Esc_exp = ProdExpFactor * ESCAPE) %>%
    mutate(`Cohort Size ANM` = Cohort - (age_nat_mort * Cohort), .after = Cohort) %>%
    rename(`Cohort Size BNM` = Cohort,
           `AEQ Rate` = aeq,
           `Mat Rate` = Mat_eq,
           `Stock Country` = stock_country) %>%
    mutate(Decade_BY = formatYrDecade(brood_year),
           Decade_CY = formatYrDecade(run_year),
           LatestRY = max(run_year, na.rm = TRUE)) %>%
    select(-age_nat_mort,
           -max_age)

  return(out_df)
}

#' Format an OUT Data frame with fishery summary fields for export
#'
#' @param out_df OUT data frame to format for exports
#'
#' @return A OUT data frame with additional columns describing the OUT data
#'
#' @importFrom dplyr mutate tibble right_join inner_join if_else case_when
#' @importFrom dplyr everything rename n
#' @importFrom tidyr pivot_longer pivot_wider
#'
formatOutFisheryExport <- function(out_df) {
  ages <- select(hrjexport::Ages, age)

  fishery_df <-
    hrjexport::fisheries %>%
    filter(!grepl("STRAY", fishery_name)) %>%
    select(fishery_id, fishery_type_1, fishery_name)

  ftype_mort_df <-
    out_df %>%
    select(stock_code,
           brood_year,
           age,
           method,
           stock_term_net_age,
           all_of(pull(fishery_df, fishery_name))) %>%
    pivot_longer(!c(stock_code, brood_year, age, method, stock_term_net_age),
                 names_to = "fishery_name",
                 values_to = "total_mort") %>%
    left_join(hrjexport::fisheries, by=c("fishery_name"))

  preterm_term_mort_df <-
    ftype_mort_df %>%
    mutate(new_col_name = case_when(gear_type == NetGearType &
                                      water_type == MarineWaterType &
                                      age >= stock_term_net_age ~ paste("Term", country) ,
                                    fishery_type_1 == PreTerminalFTCode ~ paste("Preterm", country),
                                    fishery_type_1 == TerminalFTCode ~ paste("Term", country))) %>%
    group_by(stock_code, brood_year, age, method, new_col_name) %>%
    summarize(new_col_value = sum(total_mort, na.rm = TRUE)) %>%
    ungroup() %>%
    select(stock_code, brood_year, age, method, new_col_name, new_col_value) %>%
    pivot_wider(names_from = "new_col_name", values_from = "new_col_value", values_fill = 0)

  ocean_term_mort_df <-
    ftype_mort_df %>%
    mutate(new_col_name = case_when(gear_type == NetGearType &
                                      water_type == MarineWaterType &
                                      fishery_type_1 != TerminalFTCode &
                                      age >= stock_term_net_age ~ "Preterm Ocean Net",
                                    fishery_type_1 == TerminalFTCode ~ "True Terminal Mortality",
                                    TRUE ~ "Ocean Mortality")) %>%
    select(-stock_term_net_age) %>%
    group_by(stock_code, brood_year, age, method, new_col_name) %>%
    summarize(new_col_value = sum(total_mort, na.rm = TRUE)) %>%
    ungroup() %>%
    select(stock_code, brood_year, age, method, new_col_name, new_col_value) %>%
    pivot_wider(names_from = "new_col_name", values_from = "new_col_value", values_fill = 0)

  out_df <-
    out_df %>%
    select(-stock_term_net_age) %>%
    left_join(preterm_term_mort_df, by=c("stock_code", "brood_year", "age", "method")) %>%
    left_join(ocean_term_mort_df, by=c("stock_code", "brood_year", "age", "method")) %>%
    infillColumn("Preterm Ocean Net", 0L) %>%
    mutate(`Total Terminal Mortality` = `Preterm Ocean Net` + `True Terminal Mortality`,
           `Mature Term Run` = `Total Terminal Mortality` + ESCAPE,
           TotEst = `Ocean Mortality` + `Total Terminal Mortality` + ESCAPE,
           CohSurvival = TotEst / `Cohort Size ANM`,
           ER_PreTerminal = if_else(`Cohort Size ANM` <= `Ocean Mortality`,
                                    `Ocean Mortality`/(`Cohort Size ANM` + 1),
                                    `Ocean Mortality`/`Cohort Size ANM`),
           Survival_fromFisheries = 1 - ER_PreTerminal,
           MRE_Preterm_annual_est = `Ocean Mortality` * `Mat Rate`,
           AEQ_Preterm = `Ocean Mortality` * `AEQ Rate`,
           TermMort_exp = ProdExpFactor * `Total Terminal Mortality`,
           MRE_Preterm_annual_exp = ProdExpFactor * MRE_Preterm_annual_est,
           AEQ_Preterm_exp = ProdExpFactor * AEQ_Preterm,
           `Terminal Survival` = if_else(`Mature Term Run` == 0,
                                         NA_real_,
                                         1 - (`Total Terminal Mortality` / `Mature Term Run`)),
           `Marine Survival` = `Cohort Size BNM` / cwt_rel_total) %>%
    select(-`TOTAL CATCH`, -`TOTAL RECVRS`)

  return(out_df)
}


#' Read the OUT files from a directory (recursively), checks the data, and write the results to Excel
#'
#' @param out_file_path Directory or file name of the OUT file(s)
#' @param output_dir Directory to write the export file
#' @param output_filename File Name of the excel file to write, if NULL then it will be generated
#' @param limit_method Limit data to a particular method(s) (e.g. Calendar and/or Brood)
#'
#' @return List with the calendar_file_name, brood_file_name, and combined_file_name
#'
#' @export
#'
#' @importFrom dplyr count distinct setdiff if_else case_when left_join summarize desc
#' @importFrom glue glue glue_data glue_collapse
#' @importFrom fs is_file path_dir
#'
#' @examples
#' exportOutExcelFiles(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"),
#'                     output_dir = tempdir())
#'
exportOutExcelFiles <- function(out_file_path = NA,
                                output_dir = NA,
                                output_filename = NA,
                                limit_method = OutMthdValid) {
  if(is.na(output_filename)) {
    output_filename <- glue("OUT File Export {getTimeStampText()}.xlsx")
  }

  if(is.na(output_dir)) {
    if(fs::is_file(out_file_path)) {
      output_dir <- fs::path_dir(out_file_path)
    } else {
      output_dir <- out_file_path
    }
  }

  excel_file_name <-
    exportOutFiles(out_file_path, limit_method) %>%
    split(.$method) %>%
    writeOutExcelFile(output_dir,
                      output_filename)

  return(excel_file_name)
}

#' Read the OUT files from a directory (recursively), checks the data, and return data frame of the export
#'
#' @param out_file_path Directory or file name of the OUT file(s)
#' @param limit_method Limit data to a particular method (e.g. Calendar or Brood)
#'
#' @return A data frame with the OUT file data in export format
#'
#' @export
#'
#' @importFrom dplyr count distinct setdiff if_else case_when left_join summarize desc
#' @importFrom glue glue glue_data glue_collapse
#'
#' @examples
#' exportOutFiles(system.file("extdata", "CHI_SAMPLE.zip", package = "hrjexport"))
#'
exportOutFiles <- function(out_file_path = NA,
                           limit_method = OutMthdCalendar) {

  out_df <- readOutFiles(out_file_path, limit_method)

  result_df <-
    out_df %>%
    formatOutStockExport() %>%
    formatOutFisheryExport() %>%
    arrange(method, stock_code, brood_year, desc(age))

  return(result_df)
}
