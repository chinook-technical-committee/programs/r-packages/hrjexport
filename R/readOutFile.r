
OutFisheryColSize <- 7L

#' Identifier for the Calendar Year method for OUT Files
#'
#' @export
OutMthdCalendar <- "CALENDAR YEAR METHOD"

#' Identifier for the Calendar Year method for OUT Files
#'
#' @export
OutMthdBrood <- "BROOD YEAR METHOD"

#' All valid method identifiers for OUT Files
#'
#' @export
OutMthdValid <- c(OutMthdCalendar, OutMthdBrood)

#' Parse fishery names from OUT file text segments
#'
#' @param out_segments List of OUT file text segments
#'
#' @return A vector of fishery names
#'
#' @importFrom stringr str_extract_all
#' @importFrom dplyr select full_join
#' @importFrom utils head
#'
parseOutFisheryNames <- function(out_segments) {
  fishery_len <- 82
  fishery_name_ends <- (1:fishery_len * OutFisheryColSize)
  fishery_name_starts <- head(c(0, fishery_name_ends) + 1, -1)

  fishery_prefix <- substring(out_segments[[6]][2], 15)
  fishery_prefix <- trimws(substring(fishery_prefix, fishery_name_starts, fishery_name_ends))

  fishery_names <- substring(out_segments[[6]][3], 15)
  fishery_names <- trimws(substring(fishery_names, fishery_name_starts, fishery_name_ends))

  fishery_suffix <- substring(out_segments[[6]][4], 15)
  fishery_suffix <- trimws(substring(fishery_suffix, fishery_name_starts, fishery_name_ends))

  fishery_names <- trimws(paste(fishery_prefix, fishery_names, fishery_suffix))

  alt_fishery_names <-
    fisheries %>%
    filter(exported == "Y") %>%
    pull(fishery_name)

  if (length(alt_fishery_names) != 77L) {
    stop("Length of fishery names is a issue.")
  }

  fishery_names[1:77] <- alt_fishery_names

  return(fishery_names)
}

#' Parse Stock header information from OUT file text segments
#'
#' @param out_segments List of OUT file text segments
#'
#' @return A data frame of stock header
#'
#' @importFrom stringr str_extract_all
#' @importFrom dplyr select full_join
#'
parseOutStockHeader <- function(out_segments) {
  stock_code <- substr(out_segments[[1]][1], 1, 3)

  valid_stock <-
    pull(hrjexport::Stocks, stock_code) %>%
    intersect(stock_code)

  if (length(valid_stock) != 1) {
    warn_msg <- glue("WARNING - Unknown stock code '{stock_code}'\r\n")
    cat(warn_msg)
  }

  method <- out_segments[[1]][2]
  rel_size_line <- out_segments[[5]][2]

  stock_term_net_age <-
    gsub("ALL NET CATCH AT AGE (\\d) OR OLDER IS TREATED AS TERMINAL CATCH",
       "\\1",
       out_segments[[length(out_segments)]]) %>%
    as.integer()

  if (all(is.na(stock_term_net_age))) {
    stop("Could not parse the age for the terminal net catch")
  } else {
    stock_term_net_age <- max(stock_term_net_age, na.rm = TRUE)
  }

  totals <- str_extract_all(rel_size_line, "\\d{1,}", simplify = TRUE)

  if (length(totals) != 3) {
    stop("Release size line values don't match expected length")
  }

  stock_header_df <-
    tibble(stock_code = stock_code,
           brood_year = as.integer(totals[3]) + 1900L,
           method = method,
           cwt_rel_total = as.integer(totals[1]),
           rel_total = as.integer(totals[2]),
           stock_term_net_age = stock_term_net_age)

  return (stock_header_df)
}

#' Parse a stock mortality table from OUT file text segments
#'
#' @param mort_segment A vector of character lines of the mortality table
#' @param fishery_names Vector of fishery names
#'
#' @return A data frame of stock mortality
#'
#' @importFrom dplyr select full_join across
#' @importFrom stringr str_replace_all
#'
parseOutStockMortality <- function(mort_segment, fishery_names) {
  data_col_names <- c("age", "Cohort", fishery_names)
  fishery_df <- NULL

  fishery_len <- 84L
  fishery_name_ends <- (1:fishery_len * OutFisheryColSize)
  fishery_name_starts <- head(c(0, fishery_name_ends) + 1, -1)

  #Identify which lines start with "   AGE #..." that are mortality lines
  age_line <- grepl("[ ]{2}AGE [1-6]{1}", mort_segment)

  for(row_idx in 1:length(mort_segment)) {
    single_data_row <- mort_segment[[row_idx]]

    if(age_line[row_idx] == TRUE) {
      #Line is an age specific set of values
      if (fishery_len * OutFisheryColSize < nchar(single_data_row)) {
        stop(glue("Fishery Line is too long:\n{single_data_row}\n"))
      }

      single_data_row <- trimws(substring(single_data_row, fishery_name_starts, fishery_name_ends))
      names(single_data_row) <- data_col_names
      fishery_df <- bind_rows(fishery_df, single_data_row)
    }
  }

  fishery_df <-
    fishery_df %>%
    mutate(age = as.integer(str_replace_all(age, "AGE ", ""))) %>%
    mutate(across(where(is.character), as.double))

  return (fishery_df)
}

#' Read a single OUT file
#'
#' @param out_file_name File name of the OUT file to read
#' @param limit_methods Limit reading of OUT files a specific method
#'
#' @return A data frame of stock mortality
#'
#' @export
#'
#' @importFrom dplyr %>% tibble mutate filter inner_join bind_rows bind_cols right_join
#' @importFrom dplyr select full_join cross_join
#'
#' @examples
#' readSingleOutFile(file.path(system.file("extdata", package = "hrjexport"), "COW100CYR.OUT"))
#'
readSingleOutFile <- function(out_file_name, limit_methods = OutMthdValid) {

  out_con <- file(out_file_name, "r")
  out_text <-  readLines(out_con)

  close(out_con)

  #split the text into segments based on blank lines
  out_segments <- split(out_text, cumsum(out_text==""))

  stock_header_df <- parseOutStockHeader(out_segments)

  if (any(pull(stock_header_df, method) %in% limit_methods) == FALSE) {
    return(NULL)
  }

  fishery_names <- parseOutFisheryNames(out_segments)

  mort_table_idx <- 7
  catch_df <- parseOutStockMortality(out_segments[[mort_table_idx]], fishery_names)

  shaker_df <-
    parseOutStockMortality(out_segments[[10]], fishery_names) %>%
    select(-c("Cohort", "TOTAL CATCH", "CA ESC STRAY", "US ESC STRAY", "ESCAPE", "TOTAL RECVRS"))

  cnr_legal_df <- NULL
  cnr_sublegal_df <- NULL
  if(any(grepl("TOTAL CNR LEGAL MORTALITIES BY FISHERY", out_segments[[12]]))) {
    cnr_legal_df <-
      parseOutStockMortality(out_segments[[12]], fishery_names) %>%
      select(-c("Cohort", "TOTAL CATCH", "CA ESC STRAY", "US ESC STRAY", "ESCAPE", "TOTAL RECVRS"))

    cnr_sublegal_df <-
      parseOutStockMortality(out_segments[[14]], fishery_names) %>%
      select(-c("Cohort", "TOTAL CATCH", "CA ESC STRAY", "US ESC STRAY", "ESCAPE", "TOTAL RECVRS"))

    mat_rate_df <-
      parseOutStockMortality(out_segments[[16]], fishery_names) %>%
      select(age, Mat_eq = `ESCAPE`)

    aeq_rate_df <-
      parseOutStockMortality(out_segments[[17]], fishery_names) %>%
      select(age, aeq = `ESCAPE`)
  } else {
    mat_rate_df <-
      parseOutStockMortality(out_segments[[12]], fishery_names) %>%
      select(age, Mat_eq = `ESCAPE`)

    aeq_rate_df <-
      parseOutStockMortality(out_segments[[13]], fishery_names) %>%
      select(age, aeq = `ESCAPE`)
  }

  tm_df <-
    catch_df %>%
    bind_rows(shaker_df) %>%
    bind_rows(cnr_legal_df) %>%
    bind_rows(cnr_sublegal_df) %>%
    group_by(age) %>%
    summarize(across(everything(), ~ sum(.x, na.rm = TRUE))) %>%
    ungroup()

  stock_df <-
    stock_header_df %>%
    cross_join(tm_df) %>%
    inner_join(aeq_rate_df, by="age") %>%
    inner_join(mat_rate_df, by="age")

  return(stock_df)
}


#' Read OUT Files
#'
#' Read all the OUT files in directory, recursively
#'
#' @param out_file_path Directory or file of the OUT file(s)
#' @param limit_methods Limit outputs to particular methods (e.g. Brood/Calendar).
#'
#' @return A data frame of stock mortality
#' @export
#'
#' @importFrom dplyr %>% tibble as_tibble pull mutate filter bind_rows bind_cols select full_join
#' @importFrom fs file_delete dir_ls path_ext path_norm is_dir path_temp
#' @importFrom utils unzip
#'
#' @examples
#' readOutFiles(file.path(system.file("extdata", package = "hrjexport"), "CHI_SAMPLE.zip"))
#' readOutFiles(file.path(system.file("extdata", package = "hrjexport"), "COW100CYR.OUT"))
#'
readOutFiles <- function(out_file_path, limit_methods = OutMthdValid) {
  out_df <- NULL
  file_names <- NULL
  if (is_dir(out_file_path)) {
    file_names <- dir_ls(out_file_path, recurse = TRUE)
  } else {
    file_names <- path_norm(out_file_path)
  }

  file_ext <- path_ext(file_names)

  #Look for and parse OUT files within zip files
  zip_files <- file_names[grepl("zip", file_ext, ignore.case = TRUE)]
  if (length(zip_files) > 0) {
    temp_dir <- path_temp()
    for (zip_idx in 1:length(zip_files)) {
      zip_file_list <-
        unzip(zip_files[zip_idx], list = TRUE) %>%
        as_tibble()
      #Check for zip files contained within the zip file,
      #If exist, throw a warning as this is not supported.

      zip_zip_files <-
        zip_file_list %>%
        filter(grepl(".zip$", Name, ignore.case = TRUE))

      if (nrow(zip_zip_files) > 0) {
        cat(glue("WARNING - The following zip files contains zip files that will not be checked:\n{zip_files[zip_idx]}\n"))
      }

      out_zip_files <-
        zip_file_list %>%
        filter(grepl(".OUT$", Name, ignore.case = TRUE),
               !grepl("%", Name, ignore.case = TRUE)) %>%
        pull(Name)

      if (length(out_zip_files) > 0) {
        for (out_zip_idx in 1:length(out_zip_files)) {
          out_file_name <-
            unzip(zip_files[zip_idx],
                  files = out_zip_files[out_zip_idx],
                  exdir = temp_dir) %>%
            normalizePath()
          cat(paste0("Reading OUT file: ", out_file_name, "\n"))
          single_out_df <- readSingleOutFile(out_file_name, limit_methods)
          file_delete(out_file_name)
          out_df <- bind_rows(out_df, single_out_df)
        }
      }
    }
  }

  #Parse individual OUT files
  out_files <- file_names[grepl("out", file_ext, ignore.case = TRUE) &
                            !grepl("%", file_names, ignore.case = TRUE)]
  if (length(out_files) > 0) {
    for (out_idx in 1:length(out_files)) {
      out_file_name <- out_files[out_idx]
      cat(paste0("Reading OUT file: ", out_file_name, "\n"))
      single_out_df <- readSingleOutFile(out_file_name, limit_methods)
      out_df <- bind_rows(out_df, single_out_df)
    }
  }

  return(out_df)
}
