
#' Get HRJ Escapement Column by Letter
#'
#' @param df HRJ escapement data frame
#' @param col_letters Column letters to retrieve
#'
#' @return A vector the based on the column letters
#'
getEscColLetter <- function(df, col_letters) {
  df |>
    mutate(xyz = case_when(col_letters == "a" ~ col_a,
                           col_letters == "b" ~ col_b,
                           col_letters == "c" ~ col_c,
                           col_letters == "d" ~ col_d,
                           col_letters == "e" ~ col_e,
                           col_letters == "f" ~ col_f,
                           col_letters == "g" ~ col_g,
                           col_letters == "h" ~ col_h,
                           col_letters == "i" ~ col_i,
                           col_letters == "j" ~ col_j,
                           col_letters == "k" ~ col_k,
                           col_letters == "l" ~ col_l)) |>
    pull(xyz)

}

#' Get HRJ Fishery Column by Letter
#'
#' @param df HRJ fishery data frame
#' @param col_letters Column letters to retrieve
#'
#' @return A vector the based on the column letters
#'
getFisheryColLetter <- function(df, col_letters) {
  df |>
    mutate(xyz = case_when(col_letters == "a" ~ col_a,
                           col_letters == "b" ~ col_b,
                           col_letters == "c" ~ col_c,
                           col_letters == "d" ~ col_d,
                           col_letters == "e" ~ col_e,
                           col_letters == "f" ~ col_f,
                           col_letters == "g" ~ col_g,
                           col_letters == "h" ~ col_h,
                           col_letters == "i" ~ col_i,
                           col_letters == "j" ~ col_j,
                           col_letters == "k" ~ col_k,
                           col_letters == "l" ~ col_l,
                           col_letters == "m" ~ col_m,
                           col_letters == "n" ~ col_n,
                           col_letters == "o" ~ col_o,
                           col_letters == "p" ~ col_p,
                           col_letters == "q" ~ col_q,
                           col_letters == "r" ~ col_r,
                           col_letters == "s" ~ col_s,
                           col_letters == "t" ~ col_t)) |>
    pull(xyz)
}

#' Read a single HRJ file
#'
#' @param hrj_file_name File name of the HRJ file to read
#'
#' @return A list with data frames from the HRJ file
#'
#' @importFrom dplyr %>% tibble mutate filter inner_join bind_rows bind_cols right_join
#' @importFrom dplyr select full_join
#'
#' @export
#'
readSingleHrjFile <- function(hrj_file_name) {

  stock_code <- substr(basename(hrj_file_name), 1, 3)

  valid_stock <-
    pull(hrjexport::Stocks, stock_code) %>%
    intersect(stock_code)

  if (length(valid_stock) != 1) {
    warn_msg <- glue("WARNING - Unknown stock code '{stock_code}' in HRJ file name: \n{hrj_file_name}\n")
    cat(warn_msg)
  }

  escapement <- NULL

  hrj_con <- file(hrj_file_name, "r")
  hrj_text <- readLines(hrj_con)

  close(hrj_con)

  hrj_data <- strsplit(hrj_text, " +")

  fishery_id <-
    lapply(hrj_data, (\(x) x[2])) |>
    as.integer()


  esc_list <- hrj_data[fishery_id == 0]

  max_age <- as.integer(esc_list[[1]][3])
  min_age <- max_age - ((length(esc_list[[1]]) - 3) %/% 3 ) + 1

  stock_ages <-
    hrjexport::Ages |>
    filter(age >= min_age, age <= max_age)

  esc_df <-
    tibble(brood_year = as.integer(sapply(esc_list, "[", 1L)),
           max_age = as.integer(sapply(esc_list, "[", 3L)),
           min_age =  min_age,
           col_a = as.double(sapply(esc_list, "[", 4L)),
           col_b = as.double(sapply(esc_list, "[", 5L)),
           col_c = as.double(sapply(esc_list, "[", 6L)),
           col_d = as.double(sapply(esc_list, "[", 7L)),
           col_e = as.double(sapply(esc_list, "[", 8L)),
           col_f = as.double(sapply(esc_list, "[", 9L)),
           col_g = as.double(sapply(esc_list, "[", 10L)),
           col_h = as.double(sapply(esc_list, "[", 11L)),
           col_i = as.double(sapply(esc_list, "[", 12L)),
           col_j = as.double(sapply(esc_list, "[", 13L)),
           col_k = as.double(sapply(esc_list, "[", 14L)),
           col_l = as.double(sapply(esc_list, "[", 15L))) |>
    cross_join(stock_ages) |>
    filter(age <= max_age, age >= min_age) %>%
    mutate(total_ages = max_age - min_age + 1,
           cdn_stray_col = letters[total_ages  + age - min_age + 1],
           us_stray_col = letters[(total_ages * 2)  + age - min_age + 1],
           escapement = case_when(age == 2 ~ col_a,
                                  age == 3 ~ col_b,
                                  age == 4 ~ col_c,
                                  age == 5 ~ col_d),
           canada_stray_esc = getEscColLetter(., cdn_stray_col),
           us_stray_esc = getEscColLetter(., us_stray_col)) |>
    select(brood_year, age, escapement, canada_stray_esc, us_stray_esc)


  fishery_list <- hrj_data[fishery_id != 0]

  fishery_df <-
    tibble(brood_year = as.integer(sapply(fishery_list, "[", 1L)),
           fishery_id = as.integer(sapply(fishery_list, "[", 2L)),
           max_age = as.integer(sapply(fishery_list, "[", 3L)),
           min_age =  min_age,
           col_a = as.double(sapply(fishery_list, "[", 4L)),
           col_b = as.double(sapply(fishery_list, "[", 5L)),
           col_c = as.double(sapply(fishery_list, "[", 6L)),
           col_d = as.double(sapply(fishery_list, "[", 7L)),
           col_e = as.double(sapply(fishery_list, "[", 8L)),
           col_f = as.double(sapply(fishery_list, "[", 9L)),
           col_g = as.double(sapply(fishery_list, "[", 10L)),
           col_h = as.double(sapply(fishery_list, "[", 11L)),
           col_i = as.double(sapply(fishery_list, "[", 12L)),
           col_j = as.double(sapply(fishery_list, "[", 13L)),
           col_k = as.double(sapply(fishery_list, "[", 14L)),
           col_l = as.double(sapply(fishery_list, "[", 15L)),
           col_m = as.double(sapply(fishery_list, "[", 16L)),
           col_n = as.double(sapply(fishery_list, "[", 17L)),
           col_o = as.double(sapply(fishery_list, "[", 18L)),
           col_p = as.double(sapply(fishery_list, "[", 19L)),
           col_q = as.double(sapply(fishery_list, "[", 20L)),
           col_r = as.double(sapply(fishery_list, "[", 21L)),
           col_s = as.double(sapply(fishery_list, "[", 22L)),
           col_t = as.double(sapply(fishery_list, "[", 23L))) |>
    cross_join(stock_ages) |>
    filter(age <= max_age, age >= min_age) %>%
    mutate(total_ages = max_age - min_age + 1,
           lc_aeq_col = letters[age - min_age + 1],
           tm_aeq_col = letters[total_ages+ age - min_age + 1],
           lc_nom_col = letters[(total_ages * 2)  + age - min_age + 1],
           tm_nom_col = letters[(total_ages * 3)  + age - min_age + 1],
           cs_or_ts_col = letters[(total_ages * 4)  + age - min_age + 1],
           lc_aeq = getFisheryColLetter(., lc_aeq_col),
           tm_aeq = getFisheryColLetter(., tm_aeq_col),
           lc_nom = getFisheryColLetter(., lc_nom_col),
           tm_nom = getFisheryColLetter(., tm_nom_col),
           cs_or_ts = getFisheryColLetter(., cs_or_ts_col))|>
    select(brood_year, fishery_id, age, lc_aeq, tm_aeq, lc_nom, tm_nom, cs_or_ts)

  hrj_df <-
    fishery_df %>%
    full_join(esc_df, by = c("brood_year", "age")) %>%
    mutate(stock_code = rep(stock_code, nrow(.)))

  return(hrj_df)
}

#' Read all the HRJ files in directory, recursively
#'
#' @param hrj_file_path File or directory of the HRJ file(s)
#' @param mark_type Which type of HRJ to export, "marked" or "unmarked"
#'
#' @return A list with data frames from the HRJ file
#'
#' @export
#'
#' @importFrom dplyr %>% tibble as_tibble pull mutate filter bind_rows bind_cols select full_join
#' @importFrom fs file_delete dir_ls path_ext
#' @importFrom utils unzip
#'
#' @examples
#' readHrjFiles(file.path(system.file("extdata", package = "hrjexport"), "CHI_SAMPLE.zip"))
#' readHrjFiles(file.path(system.file("extdata", package = "hrjexport"), "COWB1.HRJ"))
#'
#'
readHrjFiles <- function(hrj_file_path, mark_type = "marked") {
  calendar_hrj <- NULL
  brood_hrj <- NULL

  file_names <- NULL

  is_dir_filenames <- is_dir(hrj_file_path)

  if (any(is_dir_filenames) != all(is_dir_filenames)) {
    stop("When reading HRJs you can only specify directories or specific files")
  }

  if (all(is_dir_filenames)) {
    file_names <- dir_ls(hrj_file_path, recurse = TRUE)
  } else {
    file_names <- path_norm(hrj_file_path)
  }

  file_ext <- path_ext(file_names)

  #Look for and parse HRJ files within zip files
  zip_files <- file_names[grepl("zip", file_ext, ignore.case = TRUE)]
  if (length(zip_files) > 0) {
    temp_dir <- path_temp()
    for (zip_idx in 1:length(zip_files)) {
      zip_file_list <-
        unzip(zip_files[zip_idx], list = TRUE) %>%
        as_tibble()
      #Check for zip files contained within the zip file,
      #If exist, throw a warning as this is not supported.

      zip_zip_files <-
        zip_file_list %>%
        filter(grepl(".zip$", Name, ignore.case = TRUE))

      if (nrow(zip_zip_files) > 0) {
        cat(glue("WARNING - The following zip files contains zip files that will not be checked:\n{zip_files[zip_idx]}\n"))
      }

      hrj_zip_files <-
        zip_file_list %>%
        filter(grepl(".hrj$", Name, ignore.case = TRUE)) %>%
        pull(Name)

      if (length(hrj_zip_files) > 0) {
        for (hrj_zip_idx in 1:length(hrj_zip_files)) {
          hrj_file_name <-
            unzip(zip_files[zip_idx],
                  files = hrj_zip_files[hrj_zip_idx],
                  exdir = temp_dir) %>%
            normalizePath()
          cat(paste0("Reading hrj file: ", hrj_file_name, "\n"))
          hrj_df <- readSingleHrjFile(hrj_file_name)
          file_delete(hrj_file_name)
          if (grepl("c1[a-z_]*.hrj$", hrj_file_name, ignore.case = TRUE)) {
            calendar_hrj <-
              bind_rows(calendar_hrj, hrj_df)
          } else if (grepl("b1[a-z_]*.hrj$", hrj_file_name, ignore.case = TRUE)) {
            brood_hrj <-
              bind_rows(brood_hrj, hrj_df)
          } else {
            stop(paste0("Unknown brood/calendar year HRJ: \"",
                        hrj_file_name,
                        "\" in \"",
                        zip_files[zip_idx],
                        "\""))
          }
        }
      }
    }
  }

  #Parse individual HRJ files
  hrj_files <- file_names[grepl("hrj", file_ext, ignore.case = TRUE)]
  if (length(hrj_files) > 0) {

    mark_suffix <-
      basename(hrj_files) |>
      tools::file_path_sans_ext()

    mark_suffix <-
      substr(mark_suffix, nchar(mark_suffix) - 1, nchar(mark_suffix)) |>
      toupper()

    if (mark_type == "marked") {
      skip_file <- mark_suffix == "_U"
    } else if (mark_type == "unmarked") {
      skip_file <- mark_suffix != "_U"
    } else {
      stop("Unknown mark type, must be \"marked\" or \"unmarked\": ", mark_type)
    }

    for (hrj_idx in 1:length(hrj_files)) {
      hrj_file_name <- hrj_files[hrj_idx]
      if(skip_file[hrj_idx] == TRUE) {
        cat(paste0("Skipping hrj file: ", hrj_file_name, "\n"))
        next
      }
      cat(paste0("Reading hrj file: ", hrj_file_name, "\n"))
      hrj_df <- readSingleHrjFile(hrj_file_name)
      if (grepl("c1[a-z_]*.hrj$", hrj_file_name, ignore.case = TRUE)) {
        calendar_hrj <-
          bind_rows(calendar_hrj, hrj_df)
      } else if (grepl("b1[a-z_]*.hrj$", hrj_file_name, ignore.case = TRUE)) {
        brood_hrj <-
          bind_rows(brood_hrj, hrj_df)
      } else {
        stop(paste0("Unknown brood/calendar year HRJ: \"",
                    hrj_file_name,
                    "\" in \"",
                    zip_files[zip_idx],
                    "\""))
      }
    }
  }

  return(list(calendar_hrj = calendar_hrj,
              brood_hrj = brood_hrj))
}
