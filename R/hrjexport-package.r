#' hrjexport
#'
#' This package incorporates general utility functions to export HRJ files
#'
#' @author Nicholas Komick
#' @name hrjexport
#'
"_PACKAGE"



#' CTC Fisheries
#'
#' @docType data
#'
#' @usage data(fisheries)
"fisheries"

#' CTC Stocks
#'
#' @docType data
#'
"Stocks"

#' Possible ages for CTC ERA
#'
#' @format A data frame with 5 rows and 2 variables:
#' \describe{
#'   \item{age}{Possible age values used with the CTC ERA}
#'   \item{age_nat_mort}{CTC natural mortality rate for each age}
#'   ...
#' }
"Ages"


#' Test data set of HRJ single fixed width data file
#'
#' @docType data
#'
#' @usage data(testSingleHrjFile)
"testSingleHrjFile"


#' Test data set of HRJ single space delimited data file
#'
#' @docType data
#'
#' @usage data(testSpaceDelHrjFile)
"testSpaceDelHrjFile"

#' Test data set of HRJ data zip file
#'
#' @docType data
#'
#' @usage data(testZipHrjFile)
"testZipHrjFile"


#' Test data set of OUT single data file
#'
#' @docType data
#'
#' @usage data(testSingleOutFile)
"testSingleOutFile"

#' Test data set of HRJ data zip file
#'
#' @docType data
#'
#' @usage data(testZipOutFile)
"testZipOutFile"

#' Test data set of OUT data zip file export
#'
#' @docType data
#'
#' @usage data(testZipOutExport)
"testZipOutExport"


#' Test data set of HRJ data zip file export
#'
#' @docType data
#'
#' @usage data(testZipHrjExport)
"testZipHrjExport"

