
#' Write HRJ data frame to a excel file
#'
#' @param hrj_df HRJ data frame
#' @param output_dir Directory to write the Excel file too
#' @param hrj_type Type is "Calendar", "Brood", or "Combine"
#' @param output_filename Use this specific file name instead of generated name
#'
#' @return Return the file name written
#'
#' @export
#'
#' @importFrom openxlsx createWorkbook addWorksheet writeDataTable saveWorkbook
#' @importFrom fs path path_abs
#' @importFrom glue glue
#' @importFrom dplyr coalesce
#'
writeSingleHrjExcelFile <- function(hrj_df, output_dir, hrj_type = NULL, output_filename = NULL) {
  if(is.null(output_filename)) {
  excel_file_name <-
    path(output_dir,
         glue("HRJ_{hrj_type}_Export_{getTimeStampText()}.xlsx")) %>%
    normalizePath(mustWork = FALSE)
  } else {
    excel_file_name <-
      path(output_dir,
           output_filename) %>%
      normalizePath(mustWork = FALSE)
  }

  hrj_df <-
    hrj_df %>%
    mutate(exported = coalesce(exported, "Y")) %>%
    filter(exported == "Y") %>%
    select(-exported)

  # write_xlsx(list("HRJ" = hrj_df),
  #            path = excel_file_name)
  wb <- createWorkbook()
  addWorksheet(wb, "HRJ")
  writeDataTable(wb, "HRJ", x = hrj_df)

  cat(glue("Writing {hrj_type} HRJ Excel Export to:\n {excel_file_name}\n\n"))

  saveWorkbook(wb, excel_file_name, overwrite = TRUE)
  return(excel_file_name)
}
