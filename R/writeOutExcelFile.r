
#' Write OUT format data to Excel
#'
#' Write OUT data frame to a excel file
#'
#' @param out_data List or a single OUT data frame
#' @param output_dir Directory to write the Excel file too
#' @param output_filename Use this specific file name instead of generated name
#'
#' @return Return the file name written
#'
#' @export
#'
#' @importFrom openxlsx createWorkbook addWorksheet writeDataTable saveWorkbook
#' @importFrom fs path path_abs
#' @importFrom glue glue
#' @importFrom dplyr coalesce
#'
writeOutExcelFile <- function(out_data, output_dir = getwd(), output_filename = NULL) {
  if(is.null(output_filename)) {
    excel_file_name <-
      path(output_dir,
           glue("OUT_Export_{getTimeStampText()}.xlsx")) %>%
      normalizePath(mustWork = FALSE)
  } else {
    excel_file_name <-
      path(output_dir,
           output_filename) %>%
      normalizePath(mustWork = FALSE)
  }

  wb <- createWorkbook()
  if (is.data.frame(out_data)) {
    addWorksheet(wb, "OUT")
    writeDataTable(wb, "OUT", x = out_data)
  } else if (is.list(out_data)) {
    out_names <- names(out_data)

    for (idx in 1:length(out_names)) {
      addWorksheet(wb, out_names[idx])
      writeDataTable(wb, out_names[idx], x = out_data[[idx]])
    }
  }

  cat(glue("Writing OUT Excel Export to:\n {excel_file_name}\n\n"))

  saveWorkbook(wb, excel_file_name, overwrite = TRUE)

  return(excel_file_name)
}
