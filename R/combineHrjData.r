

#' Check that the Brood Year and Calendar Year HRJ data can be combined
#'
#' @param hrj_list HRJ data list with brood and calendar data frames
#'
#' @importFrom dplyr count distinct setdiff
#' @importFrom glue glue glue_data glue_collapse
#'
checkHrjData <- function(hrj_list) {
  error_found <- FALSE
  brood_hrj_df <- hrj_list$brood_hrj
  calendar_hrj_df <- hrj_list$calendar_hrj

  #checks for uniqueness within and between the two data frames

  non_uniq_brood <-
    brood_hrj_df %>%
    count(stock_code, fishery_id, brood_year, age) %>%
    filter(n > 1)

  if (nrow(non_uniq_brood) > 1) {
    msg <-
      non_uniq_brood %>%
      distinct(stock_code, fishery_id, brood_year) %>%
      glue_data("{stock_code}/{fishery_id}/{brood_year}") %>%
      glue_collapse(sep = "\n") %>%
      paste0("The following brood HRJ \"stock/fishery/brood years\" have non-unique ages:\n", .)
    cat(msg)
    error_found <- TRUE
  }

  non_uniq_cal <-
    calendar_hrj_df %>%
    count(stock_code, fishery_id, brood_year, age) %>%
    filter(n > 1)

  if(nrow(non_uniq_cal) > 1) {
    msg <-
      non_uniq_cal %>%
      distinct(stock_code, fishery_id, brood_year) %>%
      glue_data("{stock_code}/{fishery_id}/{brood_year}") %>%
      glue_collapse(sep = "\n") %>%
      paste0("The following calendar HRJ \"stock/fishery/brood years\" have non-unique ages:\n", .)
    cat(msg)
    error_found <- TRUE
  }

  brood_stock_df <-
    brood_hrj_df %>%
    distinct(stock_code, fishery_id)

  cal_stock_df <-
    calendar_hrj_df %>%
    distinct(stock_code, fishery_id)

  missing_brood_stock_df <-
    dplyr::setdiff(brood_stock_df, cal_stock_df)

  if (nrow(missing_brood_stock_df) > 0) {
    error_found <- TRUE
    msg <-
      missing_brood_stock_df %>%
      glue_data("{stock_code}/{fishery_id}") %>%
      glue_collapse(sep = "\n") %>%
      paste0("The following brood HRJ \"stock/fishery\" missing from calendar HRJ:\n", .)
    cat(msg)
  }

  missing_cal_stock_df <-
    dplyr::setdiff(cal_stock_df, brood_stock_df)

  if (nrow(missing_cal_stock_df) > 0) {
    error_found <- TRUE
    msg <-
      missing_cal_stock_df %>%
      glue_data("{stock_code}/{fishery_id}") %>%
      glue_collapse(sep = "\n") %>%
      paste0("The following calendar HRJ \"stock/fishery\" missing from brood HRJ:\n", .)
    cat(msg)
  }

  if(error_found) {
    stop("Need to fix alignment issues between the brood and calendar HRJ")
  } else {
    cat("Calendar and brood HRJ data looks good to combine.\n")
  }

}

#' Combine that the Brood Year and Calendar Year HRJ data into a single HRJ
#'
#' @param hrj_list HRJ data list with brood and calendar data frames
#' @param inc_method_col Include a column identifying if the values where from calendar or brood method
#'
#' @return A data frame of combined brood and calendar data
#'
#' @export
#'
#' @importFrom dplyr count distinct setdiff anti_join group_by
#' @importFrom dplyr summarize count semi_join arrange ungroup
#' @importFrom glue glue glue_data glue_collapse
#'
combineHrjData <- function(hrj_list, inc_method_col = TRUE) {

  checkHrjData(hrj_list)

  brood_hrj_df <- hrj_list$brood_hrj
  calendar_hrj_df <- hrj_list$calendar_hrj

  if(inc_method_col) {
    brood_hrj_df <- mutate(brood_hrj_df, method = "Brood")
    calendar_hrj_df <- mutate(brood_hrj_df, method = "Calendar")
  }

  #If a calendar stock/brood year/age has no fishery recoveries, then use brood year
  use_brood_stock <-
    calendar_hrj_df %>%
    group_by(stock_code, brood_year, age) %>%
    summarize(lc_nom = sum(lc_nom, na.rm = TRUE)) %>%
    ungroup() %>%
    filter(lc_nom == 0L)

  combine_hrj_df <-
    calendar_hrj_df %>%
    anti_join(use_brood_stock, by = c("stock_code", "brood_year", "age"))

  if (inc_method_col) {
    combine_hrj_df <- mutate(combine_hrj_df, method = "Calendar")
  }

  combine_brood_hrj_df <-
    brood_hrj_df %>%
    semi_join(use_brood_stock, by = c("stock_code", "brood_year", "age"))

  if (inc_method_col) {
    combine_brood_hrj_df <- mutate(combine_brood_hrj_df, method = "Brood")
  }

  combine_hrj_df <-
    bind_rows(combine_brood_hrj_df, combine_hrj_df) %>%
    arrange(stock_code, brood_year, fishery_id, age)

  return(combine_hrj_df)
}
