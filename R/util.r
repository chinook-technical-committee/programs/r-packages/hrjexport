#' Get Timestamp Text
#'
#' Provides a standardized text based time stamp for inclusion in file names
#'
#' @return A string with the current date and time
#'
#' @export
#'
getTimeStampText <- function() {
  current_time <- Sys.time()
  return(format(current_time, "%Y%m%d_%H%M%S"))
}


#' Infill Columns
#'
#' Adds a column if it doesn't exist in a form that is compatible with pipes
#'
#' @param df Data frame to add the column to
#' @param col_names A single or vector of column names add if missing
#' @param default Value to populate the new column or the variable type to use
#'
#' @return The data frame with the column added
#'
#' @export
#'
infillColumn <- function(df, col_names, default) {
  col_name_len <- length(col_names)
  for (idx in 1:col_name_len) {
    col_name <- col_names[idx]
    if (!exists(col_name, where = df)) {
      df[[col_name]] <- rep(default, times = nrow(df))
    }
  }
  return(df)
}

#' Inverse Value Matching
#'
#' Complement of `%in%`. Returns the elements of `x` that are
#' not in `y`.
#'
#' @usage x \%notin\% y
#' @param x a vector
#' @param y a vector
#' @export
#' @rdname notin
"%notin%" <- function(x, y) { return(!x %in% y) }
